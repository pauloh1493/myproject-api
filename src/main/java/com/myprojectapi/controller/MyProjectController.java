package com.myprojectapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myprojectapi.model.CategoriaEntity;
import com.myprojectapi.repository.CategoriaRepository;


@RestController
@RequestMapping("/categorias")
public class MyProjectController {

	@Autowired
	private CategoriaRepository catRep;
	
	@GetMapping
	public List<CategoriaEntity> listar(){
		return catRep.findAll();
	}
}
